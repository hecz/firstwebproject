package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@RequestMapping("/")
public class MainController {
	@RequestMapping(method = RequestMethod.GET)
	public String mainGet(ModelMap model) {
//		model.addAttribute("message", "Hello world!");
		return "index";
	}

    @RequestMapping(method = RequestMethod.POST)
    public String mainPost(ModelMap model) {

        return "index";
    }
}


