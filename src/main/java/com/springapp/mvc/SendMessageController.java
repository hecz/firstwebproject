package com.springapp.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * Created by ruslan on 23.10.2014.
 */
@Controller
@RequestMapping("/hello")

public class SendMessageController {

    @RequestMapping(method = RequestMethod.POST)
    public String GetMassage(@RequestParam String name,@RequestParam String phone,@RequestParam String email,@RequestParam String message,HttpServletRequest request,ModelMap model) throws UnsupportedEncodingException, UnknownHostException {

//        String name    = request.getParameter("name");
//        String phone   = request.getParameter("phone");
//        String email   = request.getParameter("email");
//        String message = request.getParameter("message");

        model.addAttribute("name", name);
        model.addAttribute("phone", phone);
        model.addAttribute("email", email);
        model.addAttribute("message", message);
        Sender sender = new Sender("heczua@gmail.com", "fljkmaubnkth");
        sender.send("The massege from site: ", " Username : "+name+"\n Phone: "+phone+"\n e-mail:"+email+"\n Massage:"+message, "heczua@gmail.com", "tee-2003ok@mail.ru");

        MongoSaver.save(name,phone,email,message);

        return "hello";
    }
}