<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<html lang="ru">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Яковлев Руслан Александрович</title>

    <!-- Bootstrap Core CSS -->
    <link href="<c:url value="/resources/css/bootstrap.css"/>" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<c:url value="/resources/css/stylish-portfolio.css"/>" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<c:url value="/resources/font-awesome-4.1.0/css/font-awesome.min.css"/>" rel="stylesheet"
          type="text/css">
    <link href=http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
          rel="stylesheet" type="text/css">
    <!-- jQuery Version 1.11.0 -->
    <script src="<c:url value="/resources/js/jquery-1.11.0.js"/>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.js"/>"></script>
    <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <script src="http://malsup.github.com/jquery.form.js"></script>

    <![endif]-->

</head>

<body>
<!-- Navigation -->
<a id="menu-toggle" href="#" class="btn btn-dark btn-lg toggle"><i class="fa fa-bars"></i></a>
<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <a id="menu-close" href="#" class="btn btn-light btn-lg pull-right toggle"><i class="fa fa-times"></i></a>
        <li class="sidebar-brand">
            <a href="#top">Главная</a>
        </li>
        <li>
            <a href="#top">Домашняя</a>
        </li>
        <li>
            <a href="#about">О себе</a>
        </li>
        <li>
            <a href="#services">Навыки</a>
        </li>
        <li>
            <a href="#portfolio">Опыт работы</a>
        </li>
        <li>
            <a href="#contact">Контакты</a>
        </li>
        <li>
            <a href="#" onclick="showModal()">Отправить сообщение</a>
        </li>
    </ul>
</nav>

<!-- Header -->
<header id="top" class="header">
    <div class="text-vertical-center">
        <h2><b>Яковлев Руслан Александрович</b></h2>

        <h3>Резюме &amp; Портфолио</h3>
        <br>
        <a href="#about" class="btn btn-dark btn-lg">Узнать больше</a>
    </div>
</header>

<!-- About -->
<section id="about" class="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Я работаю чтобы ваш бизнесс стал лучше!</h2>

                <p class="lead">Огромный опыт внедрения программ семейства <a target="_blank" href="http://1C.ru">1C</a>.
                </p>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<!-- Services -->
<!-- The circle icons use Font Awesome's stacked icon classes. For more information, visit http://fontawesome.io/examples/ -->
<section id="services" class="services bg-primary">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-10 col-lg-offset-1">
                <h2>Навыки и умения</h2>
                <hr class="small">
                <div class="row">
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-bar-chart-o fa-stack-1x text-primary"></i>
                            </span>
                            <h4>
                                <strong>1С программирование</strong>
                            </h4>

                            <p>Написание программ на 1С это захватывающее путешествие в мир современных технологий.</p>
                            <a href="#" class="btn btn-light">Подробнее</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-graduation-cap fa-stack-1x text-primary"></i>
                            </span>
                            <h4>
                                <strong>Обучение</strong>
                            </h4>

                            <p>Общение с людьми и передача опыта помогает двигаться вперед.</p>
                            <a href="#" class="btn btn-light">Подробнее</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-cogs fa-stack-1x text-primary"></i>
                            </span>
                            <h4>
                                <strong>Крупные внедрения</strong>
                            </h4>

                            <p>Крупный бизнес не может обойтись без современной учетной системы.</p>
                            <a href="#" class="btn btn-light">Подробнее</a>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="service-item">
                                <span class="fa-stack fa-4x">
                                <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-code fa-stack-1x text-primary"></i>
                            </span>
                            <h4>
                                <strong>Java</strong>
                            </h4>

                            <p>Изучение новых технологий помогает решать задачи быстрее и эффективнее.</p>
                            <a href="#" class="btn btn-light">Подробнее</a>
                        </div>
                    </div>
                </div>
                <!-- /.row (nested) -->
            </div>
            <!-- /.col-lg-10 -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</section>

<!-- Callout -->
<aside class="callout">
    <div class="text-vertical-center">
        <h1>Профессионализм – залог качества!</h1>
    </div>
</aside>

<!-- Portfolio -->
<section id="portfolio" class="portfolio">
    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header text-center">
                    Опыт работы
                </h1>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default panel">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-male "></i> Ведущий программист</h4>
                    </div>
                    <div class="panel-body" style="height: 400px;">
                        <h4>УК МД ГРУПП</h4>
                        <h5>май 2011 - настоящее время</h5>
                        <br>


                        <p>Доработка конфигураций под требования компании, консультирование пользователей, написание
                            инструкций, внедрение новых механизмов в учете и автоматизация многих участков работы.
                            В частности:
                        <ul>
                            <li>Автоматизация выплаты заработной платы.</li>
                            <li>Автоматизация контроля за эвкайринговыми платежами.</li>
                            <li>Внедрение собственной системы расчета себестоимости в УТ 2.3.</li>
                            <li>Участие в проекте по созданию бонусно - накопительной системы и онлайн дисконта.</li>
                        </ul>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-male "></i> Ведущий программист</h4>
                    </div>
                    <div class="panel-body" style="height: 400px;">
                        <h4>ООО ЛАНЖЕРОН ОЙЛ</h4>
                        <h5>мар 2010 - фев 2011</h5>
                        <br>


                        <p>Доработка конфигураций под требования компании, консультирование пользователей, написание
                            инструкций, внедрение новых механизмов в учете и автоматизация многих участков работы.
                            В частности:
                        <ul>
                            <li>Написание системы учета для контроля нефтепродуктов на нефтебазах, контроля отгрузок с
                                учетом температуры и плотности топлива.
                            </li>
                            <li>Установка и обучения персонала работы с системе 1с Бухгалтерия.</li>
                            <li>Доработка ряда управленческих отчетов для руководства.</li>

                        </ul>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default ">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-male "></i>Программист 1С</h4>
                    </div>
                    <div class="panel-body" style="height: 400px;">
                        <h4>НПФ АЛЬЯНС (<a class="" href="http://soft.od.ua">http://soft.od.ua </a>)</h4>
                        <h5>май 2007 - мар 2010</h5>
                        <br>


                        <p>Доработка конфигураций под требования компании, консультирование пользователей, написание
                            инструкций, внедрение новых механизмов в учете и автоматизация многих участков работы.
                            В частности:
                        <ul>
                            <li>Внедрение системы 1С на в ряде фирм в Одессе.</li>
                            <li>Получения опыта переговоров с клиентами и формализации поставленных задач.</li>
                            <li>Написание технического задания.</li>

                        </ul>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- /.row -->
    </div>

</section>

<!-- Call to Action -->
<aside class="call-to-action bg-primary">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center ">
                <a type="button" class="btn btn-lg btn-info" href=skype:hecz88"><i class="fa fa-1x fa-skype">
                    Позвонить</i> </a>
                <a type="button" class="btn btn-lg btn-success" href="#" id="clickMe" onclick="showModal()"><i
                        class="fa fa-1x fa-envelope"> Оправить сообщение</i> </a>

            </div>
        </div>
    </div>
</aside>

<section class="map" id="contact">
    <iframe width="100%" height="80%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
            src="https://www.google.com/maps/embed?pb=!1m24!1m12!1m3!1d21983.16902182428!2d30.699534105504526!3d46.470565417834614!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m9!1i0!3e6!4m0!4m5!1s0x40c6321e1576289d%3A0xd7c55ce0de1d86ca!2z0JHQuNC30L3QtdGBINGG0LXQvdGC0YAgItCc0LXRgNC60YPRgNC40LkiLCDQkdGD0LPQsNC10LLRgdC60LDRjyDRg9C7LiwgMzUsINCe0LTQtdGB0LAsINCe0LTQtdGB0YzQutCwINC-0LHQu9Cw0YHRgtGMLCA2NTAwMA!3m2!1d46.47233!2d30.702484!5e0!3m2!1sru!2sua!4v1413880884043"></iframe>
    <br/>
    <small>
        <a href="https://www.google.com.ua/maps/dir//%D0%91%D0%B8%D0%B7%D0%BD%D0%B5%D1%81+%D1%86%D0%B5%D0%BD%D1%82%D1%80+%22%D0%9C%D0%B5%D1%80%D0%BA%D1%83%D1%80%D0%B8%D0%B9%22,+%D0%91%D1%83%D0%B3%D0%B0%D0%B5%D0%B2%D1%81%D0%BA%D0%B0%D1%8F+%D1%83%D0%BB.,+35,+%D0%9E%D0%B4%D0%B5%D1%81%D0%B0,+%D0%9E%D0%B4%D0%B5%D1%81%D1%8C%D0%BA%D0%B0+%D0%BE%D0%B1%D0%BB%D0%B0%D1%81%D1%82%D1%8C,+65000/@46.4705654,30.6995341,14z/data=!4m12!1m3!3m2!1s0x0:0xd7c55ce0de1d86ca!2z0JHQuNC30L3QtdGBINGG0LXQvdGC0YAgItCc0LXRgNC60YPRgNC40Lki!4m7!1m0!1m5!1m1!1s0x40c6321e1576289d:0xd7c55ce0de1d86ca!2m2!1d30.702484!2d46.47233"></a>
    </small>
    </iframe>
</section>

<!--</div>-->
<div align="center">
    <h4><strong>Яковлев Руслан Александрович</strong>
    </h4>

    <p>Одесса<br>ул.Бугаевкая, 35 БЦ "Меркурий"</p>
    <ul class="list-unstyled">
        <li><i class="fa fa-phone fa-fw"></i> (093) 683-93-34</li>
        <li><i class="fa fa-envelope-o fa-fw"></i> <a href="mailto:heczua@gmail.com">heczua@gmail.com</a>
        </li>
    </ul>


    <!--</footer>-->

    <ul class="list-inline text-center">
        <li>
            <a href="#"><i class="fa fa-facebook fa-fw fa-3x"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-twitter fa-fw fa-3x"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-dribbble fa-fw fa-3x"></i></a>
        </li>
        <li>
            <a href="#"><i class="fa fa-vk fa-fw fa-3x"></i></a>
        </li>
    </ul>
</div>
<footer style="text-align: center">


    <p class="text-muted text-center">Copyright &copy; RuslanYakovlev.info 2014</p>
</footer>


<!-- Custom Theme JavaScript -->
<script>
    // Closes the sidebar menu
    $("#menu-close").click(function (e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Opens the sidebar menu
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });

    // Scrolls to the selected menu item on the page
    $(function () {
        $('a[href*=#]:not([href=#])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
</script>


<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
<style type="text/css">
    /*html, body { height:100%; width:100%; } *//* Для примера растягиваем окно на 100% высоту и ширину */
    .modalWindow {
        display: none;
        background: #ffffff;
        width:500px;
        height:530px;
        border:2px solid #777777;
        box-shadow: 0 0 4px #000;
        position: fixed;
        top: 15%;
        left: 35%;
        *margin-left:-100px; z-index: 1;
    }

</style>
<!-- Наш JS -->
<script type="text/javascript">
    function showModal() { // Что будет происходить по клику по ссылке
        $('.modalWindow').fadeIn('slow');
    }
    ;

</script>
<form action="/hello" method="post"  commandName="send_message" >
    <div class="modalWindow modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"
                    onclick=" $('.modalWindow').fadeOut('slow');">&times;</button>
            <h4>Сообщение</h4>
        </div>
        <%--<div class="container">--%>
        <div class="modal-body">
                <div class="row-fluid form-group">
                    Ваше имя:
                    <input class="form-control" id="name" path ="name" name="name" type="text" size="20" maxlength="255" value=""/>
                    Телефон
                    <input class="form-control" name="phone" type="text" size="20" maxlength="255" value=""/>
                    Ваш e-mail:
                    <input name="email" type="text" size="20" maxlength="255" value="" class="form-control"/>
                    Ваше сообщение
                    <textarea class="form-control" cols="80" rows="8" id="text" name="message"></textarea>

                </div>

                <div class="modal-footer">
                    <button type="submit" name="send_message" class="btn btn-lg btn-primary" id="registerModalsubmit" onclick="dawn()">
                        Отправить
                    </button>
                </div>
        </div>
        <%--</div>--%>
    </div>
</form>

</body>

</html>
